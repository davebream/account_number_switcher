var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 var bowerDirBootstrap = "vendor/bower_components/bootstrap-sass-official/assets/";
 var bowerDirBootswatch = "vendor/bower_components/bootswatch-sass";

elixir(function(mix) {
    mix.sass('app.scss');
    mix.browserify('./resources/js/main.js');
});
