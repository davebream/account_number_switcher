import React from 'react';
import ReactDOM from 'react-dom';
import update from 'react-addons-update';
import Select from 'react-select';
import ErrorMessages from './ErrorMessages.js';
import SuccessMessages from './SuccessMessages.js';
import $ from "jquery";

class App extends React.Component {
    constructor() {
      super();
      this.state = {
        firstReservationValue: '',
        secondReservationValue: '',
        messages: {
          success: [],
          error: []
        }
      };

      this.firstReservationChange = this.firstReservationChange.bind(this);
      this.secondReservationChange = this.secondReservationChange.bind(this);
      this.handleSwitchAccountsFormSubmit = this.handleSwitchAccountsFormSubmit.bind(this);
      this.resetForm = this.resetForm.bind(this);
      this.resetMessages = this.resetMessages.bind(this);
    }

    componentDidMount() {
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    }

    getOptions(input, callback){
      $.ajax({
        url: '/api/reservations',
        dataType: 'json',
        data: { number: input },
        success: function(data) {
          callback(null, {options: data.options});
        }.bind(this),
        error: function(xhr, status, err) {
          console.error(this.url, status, err.toString());
        }
      });
    }

    firstReservationChange(value) {
      this.setState({ firstReservationValue: value });
    }

    secondReservationChange(value) {
      this.setState({ secondReservationValue: value });
    }

    handleSwitchAccountsFormSubmit(e) {
      e.preventDefault();
      var url = '/api/switch-accounts';
      var data = {
        first_id: this.state.firstReservationValue.value,
        second_id: this.state.secondReservationValue.value
      };

      if(confirm('Czy napewno chcesz zamienić konta?')) {
        $.ajax({
          url: url,
          method: 'POST',
          dataType: 'json',
          data: data
        })
        .done(function(data) {
          this.resetMessages();
          let messages = update(this.state.messages, { success: {$push: [data.message] } });
          this.setState({ messages: messages}, () => { this.resetForm() });
        }.bind(this))
        .fail(function(xhr, status, err) {
          this.resetMessages();
          let messages = update(this.state.messages, { error: {$push: [xhr.responseJSON.error] } });
          this.setState({ messages: messages});
        }.bind(this));
      }
    }

    resetForm() {
      this.setState({
        firstReservationValue: '',
        secondReservationValue: ''
      });
    }

    resetMessages() {
      this.setState({ messages: { error: [], success: [] } });
    }

    render() {
        return(
          <div>
            <form onSubmit={this.handleSwitchAccountsFormSubmit}>
              <div className="row">
                <div className="form-group col-md-6">
                  <h5>Rezerwacja 1</h5>
                  <Select.Async
                    name="reservation-1"
                    onChange={this.firstReservationChange}
                    value={this.state.firstReservationValue}
                    loadOptions={this.getOptions}
                    placeholder="Wybierz rezerwację"
                    noResultsText="Nie znaleziono rezerwacji"
                    searchingText="Wyszukiwanie..."
                  />
                </div>
                <div className="form-group col-md-6">
                  <h5>Rezerwacja 2</h5>
                  <Select.Async
                    name="reservation-2"
                    onChange={this.secondReservationChange}
                    value={this.state.secondReservationValue}
                    loadOptions={this.getOptions}
                    placeholder="Wybierz rezerwację"
                    noResultsText="Nie znaleziono rezerwacji"
                    searchingText="Wyszukiwanie..."
                  />
                </div>
              </div>
              <div className="form-group">
                <button type="submit" className="btn btn-primary btn-block">
                  Zamień
                </button>
              </div>
            </form>
            {(() => {
              if (this.state.messages.error.length > 0)
                return <ErrorMessages messages={this.state.messages.error}/>
              if (this.state.messages.success.length > 0)
                return <SuccessMessages messages={this.state.messages.success}/>
            })()}
          </div>
        );
    }
}

export default App;
