import React from 'react';

class SuccessMessages extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-12">
          <div>
            { this.props.messages.map(function(message, i){
              return (
                <div key={i} className="alert alert-success" role="alert">
                  <span className="sr-only">Message:</span>
                  {message}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
};

SuccessMessages.propTypes = {
    messages: React.PropTypes.array
};

export default SuccessMessages;
