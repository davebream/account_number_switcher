import React from 'react';

class ErrorMessages extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-12">
          <div>
            { this.props.messages.map(function(error, i){
              return (
                <div key={i} className="alert alert-danger" role="alert">
                  <span className="sr-only">Error:</span>
                  {error}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
};

ErrorMessages.propTypes = {
    messages: React.PropTypes.array
};

export default ErrorMessages;
