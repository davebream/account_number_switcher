<!DOCTYPE html>
<html>
  <head>
    <title>Kaucje - Sun Seasons 24</title>
    <link href="css/app.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
  </head>
  <body>
    @yield('content')
    {{ csrf_field() }}
  </body>
</html>
