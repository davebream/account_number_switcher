@extends('master')

@section('content')
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Sun Seasons 24</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    {{ link_to_action('LoginController@logout', 'Wyloguj') }}
                </li>
              </ul>
        </div>
    </nav>
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Zamień numery kont rezerwacji</h2>
                    <div id="content"></div>
                </div>
            </div>
    </div>
    <script type="text/javascript" src="js/main.js"></script>
@stop
