<!DOCTYPE html>
<html>
  <head>
    <title>Kaucje - Sun Seasons 24</title>
    <link href="css/app.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
  </head>
  <body class="login">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="form">
                    <img src="images/logodesign.png" class="logo"/>
                    {!! Form::open(array('action' => 'LoginController@loginUser', 'method' => 'post', 'class' => 'login-form')) !!}
                        {{ Form::text('username', null, ['placeholder' => 'Użytkownik']) }}
                        {{ Form::password('password', ['placeholder' => 'Hasło']) }}
                        {{ Form::button('Zaloguj', ['type' => 'submit']) }}
                    {!! Form::close() !!}
                    @if(Session::has('message'))
                        <div class="errors">
                            <div class="alert alert-danger" role="alert">
                              {{ Session::get('message') }}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="js/main.js"></script>
    {{ csrf_field() }}
  </body>
</html>

