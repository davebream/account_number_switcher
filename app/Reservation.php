<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \DB;
use \stdClass;

class Reservation extends Model
{
    protected $table = 'rezerwacje';
    protected $primaryKey = 'rezerwacja';
    public $timestamps = false;
    protected $fillable = array('konto_rezerwacja');

    public static function getOptionsForSelect($param)
    {
        $reservations = Reservation::where('status_rezerwacja', 0);

        if (isset($param))
            $reservations = $reservations->where('numer_rezerwacja', 'LIKE', '%' . $param .'%');

        $reservations = $reservations
                        ->orderBy('rezerwacja', 'desc')
                        ->take(100)
                        ->get();

        $options = array();

        foreach ($reservations as $reservation) {
          $option = array(
            'value' => $reservation->rezerwacja,
            'label' => $reservation->numer_rezerwacja
          );

          array_push($options, $option);
        }

        $json = new stdClass();
        $json->options = $options;

        return $json;
    }

    public static function switchAccountNumbers($id1, $id2)
    {
        DB::transaction(function() use($id1, $id2) {
            try
            {
              $reservation1 = Reservation::find($id1);
              $reservation2 = Reservation::find($id2);

              $reservation1_account = $reservation1->konto_rezerwacja;
              $reservation2_account = $reservation2->konto_rezerwacja;

              $reservation1->konto_rezerwacja = $reservation2_account;
              $reservation1->save();

              $reservation2->konto_rezerwacja = $reservation1_account;
              $reservation2->save();
            }
            catch (\PDOException $e)
            {
              DB::rollback();
              return Response::json(array('code' => 400, 'message' => $e->getMessage() ), 400);
            }
        });

        DB::commit();
    }
}
