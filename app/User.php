<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'uzytkownicy';
    protected $primaryKey = 'uzytkownik';
    public $rememberToken = false;

    public function getAuthPassword()
    {
       return $this->haslo_uzytkownik;
    }

    public function getRememberToken()
    {
      return null;
    }

    public function setRememberToken($value)
    {
    }

    public function getRememberTokenName()
    {
      return null;
    }

    public function setAttribute($key, $value)
    {
      $isRememberTokenAttribute = $key == $this->getRememberTokenName();
      if (!$isRememberTokenAttribute)
      {
        parent::setAttribute($key, $value);
      }
    }
}
