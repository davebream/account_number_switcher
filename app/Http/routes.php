<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'middleware' => 'auth',
    'uses' => 'ReservationsController@home'
]);

Route::get('login', 'LoginController@login');
Route::post('login', 'LoginController@loginUser');
Route::get('logout', 'LoginController@logout');

Route::group(['prefix' => 'api', 'middleware' => 'auth'], function()
{
    Route::get('reservations', [
      'as' => 'reservations',
      'uses' => 'ReservationsController@index'
    ]);
    Route::post('switch-accounts', [
      'as' => 'switch-accounts',
      'uses' => 'ReservationsController@update'
    ]);
});
