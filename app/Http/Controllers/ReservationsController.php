<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Reservation;
use App\Http\Requests\SwitchAccountsRequest;
use Response;
use Illuminate\Http\Request;

class ReservationsController extends Controller {

    public function home()
    {
        return view('home');
    }

    public function index(Request $request)
    {
        $param = $request->input('number');
        $options = Reservation::getOptionsForSelect($param);
        return Response::json($options);
    }

    public function update(SwitchAccountsRequest $request)
    {
        $first_id = $request->first_id;
        $second_id = $request->second_id;
        $reservation1 = Reservation::find($first_id);
        $reservation2 = Reservation::find($second_id);

        Reservation::switchAccountNumbers($first_id, $second_id);

        $message = "Zamienono numery kont dla rezerwacji {$reservation1->numer_rezerwacja} i {$reservation2->numer_rezerwacja}.";
        return Response::json(['code' => 200, 'message' => $message], 200);
    }
}
