<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Auth;
use App\User;
use Response;
use Redirect;

class LoginController extends Controller {

    public function login()
    {
        return view('login');
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('login');
    }

    public function loginUser(LoginRequest $request)
    {
        $username = $request->username;
        $password = $request->password;

        $user = User::where('login_uzytkownik', $username)->first();

        if( $user && $user->getAuthPassword() == crypt($password, 'cuadro') )
        {
            Auth::attempt([
                'login_uzytkownik' => $username,
                'password' => $password,
            ]);

            return redirect()->action('ReservationsController@home');
        }
        else
        {
           return Response::json(['code' => 200, 'message' => 'Błąd logowania'], 200);
        }
    }
}
