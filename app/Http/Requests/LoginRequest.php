<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Redirect;
use Session;

class LoginRequest extends FormRequest
{
    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required'
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function response(array $errors)
    {
        Session::flash('message', "Uzupełnij pola Użytkownik i Hasło");
        return Redirect::back();
    }
}
