<?php

namespace App\Http\Requests;

use App\Reservation;
use Illuminate\Foundation\Http\FormRequest;
use Response;

class SwitchAccountsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'first_id' => 'required|numeric',
            'second_id' => 'required|numeric'
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function getValidatorInstance() {
        $validator = parent::getValidatorInstance();

        $validator->after(function() use ($validator) {
            $reservation1 = Reservation::find($this->input('first_id'));
            $reservation2 = Reservation::find($this->input('second_id'));

            if ($reservation1 === null)
               $validator->errors()->add('first_id', 'Rezerwacja nie istnieje.');

            if ($reservation2 === null)
               $validator->errors()->add('second_id', 'Rezerwacja nie istnieje.');
        });

        return $validator;
    }

    public function response(array $errors)
    {
        return Response::json(['code' => 400, 'error' => 'Brak wymaganych danych'], 400);
    }
}
